package com.develop.web.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "orders")
@ToString(exclude = "userid")
@Setter
@Getter
public class OrderInfo {
	
	@Id
	@Column(name = "orderid")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@NotBlank
	private Long orderid;
	
	@Column(name = "ordernum")
	@NotBlank
	private String ordernum;
	
	@Column(name = "productname")
	@NotBlank
	private String productname;
	
	@Column(name = "paymentdate")
	@NotBlank
	private LocalDateTime paymentdate;
	
	@Column(name = "userid")
	@NotBlank
	private Long userid;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "userid", insertable=false, updatable=false)
	private UserInfo users;
}
