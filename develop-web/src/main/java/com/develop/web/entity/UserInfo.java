package com.develop.web.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SuppressWarnings("serial")
@Entity
@Table(name = "users")
@Setter
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UserInfo implements UserDetails{
	
	@Id
	@Column(name = "userid" , unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long userid;
	
	@Column(name = "username")
	@NotEmpty(message = "사용자 이름은 필수 입력")
	@Pattern(regexp="^[가-힣]*$", message = "한글로 입력하여야 합니다.")
	private String username;
	
	@Column(name = "nickname")
	@NotEmpty(message = "닉네임은 필수 입력")
	private String nickname;
	
	@Size(min=10 , message="비밀번호는 최소 10이상부터 입력해주시기 바랍니다.")
	@Column(name = "password")
	@NotEmpty(message = "비밀번호 필수 입력")
	//@Pattern(regexp="^[a-z0-9A-Z!~@#$%^&*()?+=\\\\/]{10,20}$",
    //message = "비밀번호는 영문 대,소문자와 숫자, 특수기호가 적어도 1개 이상씩 포함된 10자 ~ 20자의 비밀번호여야 합니다.")
	private String password;
	
	@Column(name = "phonenumber")
	@NotEmpty(message = "휴대번호 필수 입력")
	@Pattern(regexp="^01(?:0|1|[6-9])(\\d{3}|\\d{4})(\\d{4})$",
    message = "올바른 휴대전화 형식입니다. (ex : 01012345678)")
	private String phonenumber;
	
	@Column(name = "email", unique = true)
	@Email
	@NotEmpty(message = "이메일은 필수 입력 값입니다.")
	@Pattern(regexp="^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[a-zA-Z]([-_.]?[a-zA-Z])*.[a-zA-Z]{2,3}$",
    message = "이메일 형식에 맞춰서 입력바랍니다.")
	private String email;
	
	@Column(name = "gender")
	private String gender;
	
	@Column(name = "auth")
	private String auth;
	
	
		
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<GrantedAuthority> roles = new HashSet<>();
		
		for (String role : auth.split(",")) {
			roles.add(new SimpleGrantedAuthority(role));
		}
		return roles;
	}
	
	//계정 만료 여부 반환
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}
	
	//계정 잠금 여부 반환
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	
	//패스워드의 만료 여부 반환
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	
	//계정 사용 가능 여부 반환
	@Override
	public boolean isEnabled() {
		return true;
	}
	
	
	//@Builder
	//public UserInfo(Long userid,
	//		@NotEmpty(message = "사용자 이름은 필수 입력") @Pattern(regexp = "^[가-힣]*$", message = "한글로 입력하여야 합니다.") String username,
	//		@NotEmpty(message = "닉네임은 필수 입력") String nickname,
	//		@Size(min = 10, message = "비밀번호는 최소 10이상부터 입력해주시기 바랍니다.") @NotEmpty(message = "비밀번호 필수 입력") @Pattern(regexp = "^[a-z0-9A-Z!~@#$%^&*()?+=\\\\\\\\/]{10,20}$", message = "비밀번호는 영문 대,소문자와 숫자, 특수기호가 적어도 1개 이상씩 포함된 10자 ~ 20자의 비밀번호여야 합니다.") String password,
	//		@NotEmpty(message = "휴대번호 필수 입력") @Pattern(regexp = "^01(?:0|1|[6-9])(\\d{3}|\\d{4})(\\d{4})$", message = "비밀번호는 영문 대,소문자와 숫자, 특수기호가 적어도 1개 이상씩 포함된 10자 ~ 20자의 비밀번호여야 합니다.") String phonenumber,
	//		@Email @NotEmpty(message = "이메일은 필수 입력 값입니다.") String email, String gender) {
	//	this.username = username;
	//	this.nickname = nickname;
	//	this.password = password;
	//	this.phonenumber = phonenumber;
	//	this.email = email;
	//	this.gender = gender;
	//}


	@OneToMany (mappedBy = "users", fetch = FetchType.LAZY)
	@JsonManagedReference
	private List<OrderInfo> orders = new ArrayList<>(); 
	
}
