package com.develop.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevelopWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevelopWebApplication.class, args);
	}

}
