package com.develop.web.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

import com.develop.web.entity.UserInfo;
import com.develop.web.repository.UserRepository;

@Service
public class UserServiceImpl implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	private BCryptPasswordEncoder encoder;

	/* 
	 * 사용자 정보 저장
	 * */
	public UserInfo save(UserInfo userDto) {
		encoder = new BCryptPasswordEncoder();

		String encodePassword = encoder.encode(userDto.getPassword());

		userDto.setPassword(encodePassword);
		userDto.setAuth("ROLE_USER");

		UserInfo userInfo = userRepository.save(userDto);
		
		return userInfo;
	}

	
	 @Override 
	 public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		
		 UserInfo userinfo = userRepository.findByEmail(email);
		 		 
		 if(userinfo == null) {
			 throw new UsernameNotFoundException("UsernameNotFound!!!");
		 }
		 
		 return userinfo;
				// User(userinfo.getUserid(), userinfo.getPassword(), Arrays.asList(new SimpleGrantedAuthority(userinfo.getAuth())));
	 }

	 
	 public Map<String, String> validateHandling (Errors errors){
		 
		 Map<String, String> validatorResult = new HashMap<>();
		 
		 for (FieldError error : errors.getFieldErrors()) {
	            String validKeyName = String.format("valid_%s", error.getField());
	            validatorResult.put(validKeyName, error.getDefaultMessage());
	        }
		 
		 return validatorResult;
	 }
	
}
