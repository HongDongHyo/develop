package com.develop.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.develop.web.entity.OrderInfo;
import com.develop.web.entity.QOrderInfo;
import com.develop.web.entity.QUserInfo;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Service
@Repository
public class OrderServiceImpl extends QuerydslRepositorySupport {
	
	public OrderServiceImpl() {
		super(OrderInfo.class);
	}
	
	@Autowired
	private JPAQueryFactory jpaQueryFactory;
		
	/* 
	 * 사용자 주문 목록
	 * */
	public List<OrderInfo> getUserOrderList(Long id) {
		
		QOrderInfo orderInfo = QOrderInfo.orderInfo;
		QUserInfo userInfo = QUserInfo.userInfo;
				
		List<OrderInfo> orderList = 
				jpaQueryFactory
				.selectFrom(orderInfo)
				.innerJoin(orderInfo.users, userInfo)
				.fetchJoin()
				.where(orderInfo.userid.eq(id))
				.orderBy(orderInfo.orderid.desc())
				.fetch();				
		
		return orderList;
	}


	/* 
	 * 사용자 최근 주문 정보
	 * */
	public OrderInfo getUseLastOrder(Long id) {
		
		QOrderInfo orderInfo = QOrderInfo.orderInfo;
		QUserInfo userInfo = QUserInfo.userInfo;

		OrderInfo userOrder = jpaQueryFactory
						.select(orderInfo)
						.from(orderInfo)
						.innerJoin(orderInfo.users, userInfo)
						.fetchJoin()
						.where(orderInfo.orderid.in(
								JPAExpressions.select(orderInfo.orderid.max())
								.from(orderInfo)
								.where(orderInfo.userid.eq(id))
								))
						.fetchFirst();				
		
		return userOrder;
	}
}
