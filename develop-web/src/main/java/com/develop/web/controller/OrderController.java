package com.develop.web.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.develop.web.entity.OrderInfo;
import com.develop.web.entity.UserInfo;
import com.develop.web.repository.UserRepository;
import com.develop.web.service.impl.OrderServiceImpl;

import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("order")
public class OrderController{
	private final Logger logger = LoggerFactory.getLogger(OrderController.class);
		
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private OrderServiceImpl orderService;
	
	/* 
	 * 사용자 주문 목록
	 * */
	@ApiOperation(value="Order List", notes="주문 리스트")
	@GetMapping("/orderlist")
	public String userOrderList(@RequestParam(required = false) String email, Model model){
		
		UserInfo userInfo = userRepository.findByEmail(email);
		
		Long userid = userInfo.getUserid();
		
		List<OrderInfo> orderInfos = null;
		
		if(userid != null) {
			orderInfos = orderService.getUserOrderList(userid);
		}
		
		model.addAttribute("orderlist", orderInfos); 
		model.addAttribute("orderCnt", orderInfos.size());
		
		return "order/orderList";
	}
	
	/* 
	 * 사용자 최근 주문 정보
	 * */
	@ApiOperation(value="User Last Order", notes="최근 주문")
	@GetMapping("/lastOrder")
	public String useLastOrder(@RequestParam(required = false) Long id, Model model) throws Exception{
		
		OrderInfo lastOrder = null;
		Optional<UserInfo> optional = null;
		UserInfo userInfo = null;
		
		if(id != null) {
			lastOrder = orderService.getUseLastOrder(id);
			optional = userRepository.findById(id);
			
			if(optional.isPresent()) {
				userInfo = optional.get();
			}else {
				throw new Exception();
			}
		}
		logger.info(lastOrder.toString());
		
		model.addAttribute("order", lastOrder); 
		model.addAttribute("user", userInfo);
		
		return "order/lastOrder";
	}
}
