package com.develop.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.develop.web.entity.UserInfo;
import com.develop.web.repository.UserRepository;
import com.develop.web.service.impl.OrderServiceImpl;
import com.develop.web.service.impl.UserServiceImpl;

import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("user")
public class UserController {
		
	@Autowired
	private UserRepository userRepository;	
	
	@Autowired
	private OrderServiceImpl orderService;
	
	@Autowired
	private UserServiceImpl userService;
	
	
	@GetMapping("/login")
	public String login() {
		return "user/login";
	}
	
	
	@GetMapping("/register")
	public String register() {
		return "user/register";
	}
	
	@PostMapping("/register")
	public String register(UserInfo user) throws Exception {
		
		userService.save(user);
		
		return "redirect:/login";
	}
	
	
	@ApiOperation(value="User List", notes="회원 리스트")
	@RequestMapping(value="/userlist", method = {RequestMethod.POST, RequestMethod.GET})
	public String userList(Model model, @PageableDefault(size = 10) Pageable pageable,
	 @RequestParam(required = false, defaultValue = "") String searchText ) {
		
		Page<UserInfo> userlist = userRepository.findByUsernameContainingOrEmailContaining(searchText, searchText, pageable);
		
		model.addAttribute("userCnt", userlist.getTotalElements()); //타임리프 get을 사용하지 않고, get을 뺀 TotalElements를 사용
				
		int startPage = Math.max(1, userlist.getPageable().getPageNumber() - 4);
		int totalPage = userlist.getTotalPages() == 0 ? 1 : userlist.getTotalPages();
				 
		int endPage = Math.min(totalPage , userlist.getPageable().getPageNumber() + 4);
		
		model.addAttribute("totalPage", totalPage);
		model.addAttribute("startPage", startPage); 
		model.addAttribute("endPage", endPage); 
		model.addAttribute("userlist", userlist);
	 
		return "user/userList"; 
	}
	
	@ApiOperation(value="User Detail", notes="회원 상세 정보")
	@GetMapping(value="/userDetail")
	public String userDetail(Model model, @RequestParam(required = false) String email ) throws Exception{
		
		UserInfo userInfo;
		
		try {
			userInfo = userRepository.findByEmail(email);
			
			model.addAttribute("user", userInfo);
		} catch (Exception e) {
			e.getStackTrace();
		}
				
		return "user/userDetail";
	}
}
