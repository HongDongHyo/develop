package com.develop.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.develop.web.entity.OrderInfo;
import com.develop.web.entity.UserInfo;
import com.develop.web.repository.UserRepository;
import com.develop.web.service.impl.OrderServiceImpl;
import com.develop.web.service.impl.UserServiceImpl;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import springfox.documentation.annotations.ApiIgnore;

@Slf4j
@RestController
@RequestMapping("/api")
public class ApiContoller {

	@Autowired
	private UserRepository userRepository;	
	
	@Autowired
	private OrderServiceImpl orderService;
	
	@Autowired
	private UserServiceImpl userService;
	
	@ApiOperation(value="User List", notes="회원 리스트")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "page", value = "Results page you want to retrieve (0..N)", dataType = "int", paramType = "query"),
        @ApiImplicitParam(name = "size", value = "Number of records per page.", dataType = "int", paramType = "query"),
        @ApiImplicitParam(name = "sort", value = "Sorting criteria in the format: property(,asc|desc).", dataType = "String", paramType = "query")
			})
	@RequestMapping(value="/userlist", method = RequestMethod.GET)
	public ResponseEntity<Page<UserInfo>> userList(Model model, 
			@ApiIgnore @PageableDefault(size = 10) Pageable pageable,
			@RequestParam(required = false, defaultValue = "") String searchText ) {
				
		Page<UserInfo> userlist = userRepository.findByUsernameContainingOrEmailContaining(searchText, searchText, pageable);
		
		
		//model.addAttribute("userCnt", userlist.getTotalElements()); //타임리프 get을 사용하지 않고, get을 뺀 TotalElements를 사용
				
		/*
		 * int startPage = Math.max(1, userlist.getPageable().getPageNumber() - 4); int
		 * totalPage = userlist.getTotalPages() == 0 ? 1 : userlist.getTotalPages();
		 * 
		 * int endPage = Math.min(totalPage , userlist.getPageable().getPageNumber() +
		 * 4);
		 */
		
		/*
		 * model.addAttribute("totalPage", totalPage); model.addAttribute("startPage",
		 * startPage); model.addAttribute("endPage", endPage);
		 * model.addAttribute("userlist", userlist);
		 */
			 
		return ResponseEntity.ok().body(userlist); 
	}
	
	@ApiOperation(value="User Detail", notes="회원 상세 정보")
	@RequestMapping(method=RequestMethod.GET, value="/userDetail")
	public ResponseEntity<UserInfo> userDetail(@Valid @RequestParam String email,
			Model model) throws Exception{
		
		UserInfo userInfo = null;
		
		try {
			userInfo = userRepository.findByEmail(email);
			
		} catch (Exception e) {
			e.getStackTrace();
		}
				
		return ResponseEntity.ok().body(userInfo);
	}
	

	@ApiOperation(value="Order List", notes="사용자 주문 리스트")
	@GetMapping(value="/orderlist")
	public List<OrderInfo> userOrderList(@Valid @RequestParam(required = false) String email,
			Model model){
		
		List<OrderInfo> orderInfos = null;
		UserInfo userInfo = null;
		Long userid = null;
		
		if(!StringUtils.isEmpty(email)) {
			userInfo = userRepository.findByEmail(email);
			
			userid = userInfo.getUserid();
		}
			
		if(userid != null) {
			orderInfos = orderService.getUserOrderList(userid);
		}	
		
		return orderInfos;
	}
	
	@ApiOperation(value="User Last Order", notes="최근 주문" )
	@GetMapping(value="/lastOrder")
	public OrderInfo useLastOrder(@Valid @RequestParam(required = false) String email, 
			Model model) throws Exception{
		
		OrderInfo lastOrder = null;
		UserInfo userInfo = null;
		
		if(!StringUtils.isEmpty(email)) {
			userInfo = userRepository.findByEmail(email);
			
			lastOrder = orderService.getUseLastOrder(userInfo.getUserid());
			
		}
				
		return lastOrder;
	}
	
	@ApiOperation(value="User Save", notes="회원 가입")
	@PostMapping(value="/register" )
	public Object register(
			@Valid @RequestBody UserInfo user, BindingResult bindingResult) throws Exception {
				
		List<String> resultMessages = new ArrayList<>();
        if(bindingResult.hasErrors()){
            List<FieldError> fieldsErrors = bindingResult.getFieldErrors();
            fieldsErrors.stream().forEach(fieldError->{
                log.info("bind error objectName = {}, fieldName = {}, error code = {}",fieldError.getObjectName(),fieldError.getField(),fieldError.getCode());
            });
            return resultMessages;
        }
        
        if(!Pattern.matches("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{10,20}$", user.getPassword())) {
        	throw new Exception("비밀번호는 영문 대,소문자와 숫자, 특수기호가 적어도 1개 이상씩 포함된 10자 ~ 20자의 비밀번호여야 합니다.");
        }
        
        UserInfo existUser = userRepository.findByEmailOrPhonenumber(user.getEmail(), user.getPhonenumber());
    	
        if( existUser != null) {
        	if(existUser.getEmail().equals(user.getEmail())) {
            	throw new Exception("이미 존재하는 이메일 입니다. 다른 이메일로 입력해주시기 바랍니다.");
            } else if(existUser.getPhonenumber().equals(user.getPhonenumber())) {
            	throw new Exception("이미 핸드폰 번호 입니다. 다른 휴대폰 번호를 입력해주시기 바랍니다.");
            }        	
        }
                
		return (UserInfo)userService.save(user);
				
	}
	
	@ApiOperation(value="User Login", notes="로그인")
	@PostMapping(value="/loginAccess")
	public void loginAccess(@Valid @RequestParam(required = false) String email, 
			@Valid @RequestParam(required = false) String password) throws Exception {
		
		
	}
	
}
