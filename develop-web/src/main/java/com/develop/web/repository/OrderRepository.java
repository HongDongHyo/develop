package com.develop.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.develop.web.entity.OrderInfo;

public interface OrderRepository extends JpaRepository<OrderInfo, Long>{
	
}
