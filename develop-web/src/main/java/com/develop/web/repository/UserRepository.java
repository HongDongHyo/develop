package com.develop.web.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.develop.web.entity.UserInfo;

public interface UserRepository extends JpaRepository<UserInfo, Long>{
		
	Page<UserInfo> findByUsernameContainingOrEmailContaining (String searchUsername, String email, Pageable pageable);
		
	UserInfo findByEmail(String email);
	
	UserInfo findByEmailOrPhonenumber(String email, String phonenumber);
}
