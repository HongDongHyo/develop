package com.develop.web.config;

import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.classmate.TypeResolver;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	private String version;
	private String title;
	//private TypeResolver typeResolver;

	@Bean
	public Docket apiV1() {
		version = "V1";
		title = "idus API " + version;
		
		return new Docket(DocumentationType.SWAGGER_2)
				.consumes(getConsumeContentTypes())
				.produces(getProduceContentTypes())
				.useDefaultResponseMessages(false)
				.groupName(version)
				.apiInfo(apiInfo(title, version))
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.develop.web.controller"))
				.paths(PathSelectors.ant("/api/**"))
				.build(); 
	}

	private Set<String> getConsumeContentTypes() {
        Set<String> consumes = new HashSet<>();
        consumes.add("application/json;charset=UTF-8");
        consumes.add("application/x-www-form-urlencoded");
        return consumes;
    }
	
	private Set<String> getProduceContentTypes() {
        Set<String> produces = new HashSet<>();
        produces.add("application/json;charset=UTF-8");
        return produces;
    }
		
	private ApiInfo apiInfo(String title, String version) {
        return new ApiInfoBuilder()
        		.title(title)
        		.description("Swagger로 생성한 API Docs")
        		.version(version)
        		.build();
    }
		
}
