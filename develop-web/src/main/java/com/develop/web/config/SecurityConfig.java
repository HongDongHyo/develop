package com.develop.web.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.develop.web.service.impl.UserServiceImpl;

import lombok.AllArgsConstructor;

@Configuration
@EnableWebSecurity
@AllArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private UserServiceImpl userService;
	
	@Bean
	public PasswordEncoder passwordEncoder() { //암호화
		return new BCryptPasswordEncoder();
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception{ //static 파일 제외
		web.ignoring().antMatchers("/css/**", "/js/**", "/imgs/**", "/templates/**", 
				"/v2/api-docs",
				"/v2/api-docs/**",
                "/configuration/ui",
                "/swagger-resources/**",
                "/configuration/security",
                "/swagger-ui.html",
                "/webjars/**");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception { //6
		http
				.authorizeRequests()// 7
				//.antMatchers("/admin/**").hasRole("ROLE_ADMIN")  // ADMIN만 접근 가능
				.antMatchers(//"/", "/user/register", 
						"/api/**").permitAll() // 누구나 접근 허용
				//.antMatchers("/user/**").hasRole("ROLE_USER") // USER, ADMIN만 접근 가능
				.anyRequest().authenticated()  // 나머지 요청들은 권한의 종류에 상관 없이 권한이 있어야 접근 가능
			.and()
			.csrf().disable()
			.formLogin().disable()
				//.formLogin()
				//.loginPage("/user/login") //로그인 페이지 링크
				//.loginProcessingUrl("/user/loginAccess")
			//.usernameParameter("email")
				//.passwordParameter("password")
			//.successHandler(successHandler())
				//.defaultSuccessUrl("/") //로그인 성공 후 리다이렉트주소
			//	.permitAll()
			//.and()
			//	.logout()
			//	.logoutSuccessUrl("/user/login") //로그아웃 성공시 리다이렉트 주소
			//	.invalidateHttpSession(true) //세션 날리기
			
		/*
		 * .and() .exceptionHandling() .accessDeniedPage("/error")
		 */
		;
	}
	
		
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
	}
	
	@Bean
	public AuthenticationSuccessHandler successHandler() {
		return new LoginSuccessHandler();
	}
}
