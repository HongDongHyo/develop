package com.develop.web.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QOrderInfo is a Querydsl query type for OrderInfo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QOrderInfo extends EntityPathBase<OrderInfo> {

    private static final long serialVersionUID = 859775049L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QOrderInfo orderInfo = new QOrderInfo("orderInfo");

    public final NumberPath<Long> orderid = createNumber("orderid", Long.class);

    public final StringPath ordernum = createString("ordernum");

    public final DateTimePath<java.time.LocalDateTime> paymentdate = createDateTime("paymentdate", java.time.LocalDateTime.class);

    public final StringPath productname = createString("productname");

    public final NumberPath<Long> userid = createNumber("userid", Long.class);

    public final QUserInfo users;

    public QOrderInfo(String variable) {
        this(OrderInfo.class, forVariable(variable), INITS);
    }

    public QOrderInfo(Path<? extends OrderInfo> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QOrderInfo(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QOrderInfo(PathMetadata metadata, PathInits inits) {
        this(OrderInfo.class, metadata, inits);
    }

    public QOrderInfo(Class<? extends OrderInfo> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.users = inits.isInitialized("users") ? new QUserInfo(forProperty("users")) : null;
    }

}

