package com.develop.web.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUserInfo is a Querydsl query type for UserInfo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUserInfo extends EntityPathBase<UserInfo> {

    private static final long serialVersionUID = 1297593932L;

    public static final QUserInfo userInfo = new QUserInfo("userInfo");

    public final StringPath auth = createString("auth");

    public final StringPath email = createString("email");

    public final StringPath gender = createString("gender");

    public final StringPath nickname = createString("nickname");

    public final ListPath<OrderInfo, QOrderInfo> orders = this.<OrderInfo, QOrderInfo>createList("orders", OrderInfo.class, QOrderInfo.class, PathInits.DIRECT2);

    public final StringPath password = createString("password");

    public final StringPath phonenumber = createString("phonenumber");

    public final NumberPath<Long> userid = createNumber("userid", Long.class);

    public final StringPath username = createString("username");

    public QUserInfo(String variable) {
        super(UserInfo.class, forVariable(variable));
    }

    public QUserInfo(Path<? extends UserInfo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUserInfo(PathMetadata metadata) {
        super(UserInfo.class, metadata);
    }

}

